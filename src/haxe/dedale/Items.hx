package dedale;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import vault.ISkin;
import dedale.skins.Delice;
import dedale.skins.Heart;

@:build(patchman.Build.di())
class Items {
  @:diExport
  public var skins(default, null): FrozenArray<ISkin>;

  public function new() {
    this.skins = FrozenArray.of(
      new Delice(),
      new Heart()
    );
  }
}