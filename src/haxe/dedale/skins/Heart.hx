package dedale.skins;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import vault.ISkin;

class Heart implements ISkin {
  public static var ID_YELLOW_HEART: Int = 104;

  public var id(default, null): Int = 1240;
  public var sprite(default, null): Null<String> = "hammer_item_special";

  public function new() {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_YELLOW_HEART + 1));
  }
}