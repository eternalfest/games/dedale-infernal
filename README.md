# Dédale Infernal

Clone with HTTPS:
```shell
git clone --recurse-submodules https://gitlab.com/eternalfest/games/dedale-infernal.git
```

Clone with SSH:
```shell
git clone --recurse-submodules git@gitlab.com:eternalfest/games/dedale-infernal.git
```
